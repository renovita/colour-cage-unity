﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {
	public int num;
    public bool active;
    public Sprite activeTexture;
    public Sprite unactiveTexture;
    public GameObject ball;

    List<Color> colorList = new List<Color>(){
             Color.red,
             Color.green,
             Color.blue,
             Color.yellow,
             new Color(255F, 0F, 0F, 0.2F),
             new Color(0F, 255F, 0F, 0.2F),
             new Color(0F, 0F, 255F, 0.2F),
             new Color(255F, 255F, 0F, 0.2F)
             };
    //GetComponent<SpriteRenderer>().color = new Color(0, 0.30196078f, 0.30196078f, 1);


    // Use this for initialization
    void Start () {
        if (num == 1)
        {
            active = false;
        }
        else { active = true; }
        flop(1);
 
    }

   // void OnMouseUpAsButton()
   public void flop(int nu) { 
        if (active && nu!=num)
        {
            active = false;
            //GetComponent<SpriteRenderer>().sprite = unactiveTexture;
            //Transform ball=Instantiate(ballprefab) as Transform;
            GetComponent<SpriteRenderer>().color = colorList[num+3];
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), ball.GetComponent<Collider2D>());
        }
        else if (nu==num && !active) {
            //GetComponent<SpriteRenderer>().sprite = activeTexture;
            GetComponent<SpriteRenderer>().color = colorList[num -1];
            active = true;
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), ball.GetComponent<Collider2D>(), false);
        }
    }

   /* public void switching(int n, int num) {
        print(n);

        if (num == n) {
                flop();
        }
    }*/
}
