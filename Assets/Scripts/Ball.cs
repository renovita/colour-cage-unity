﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public float speed = 3;
    Vector2 pos;
    public Switch c;

    // Use this for initialization
    void Start () {
        GetComponent<SpriteRenderer>().color = new Color(0, 0.30196078f, 0.30196078f, 1);
        Vector2 dir = new Vector2(Random.value, Random.value).normalized;
        GetComponent<Rigidbody2D>().velocity = dir * speed;
    }
	
	//Update is called once per frame
	void Update () {
        speed*=1.01f;
        GetComponent<Rigidbody2D>().velocity *= 1.001f ;
        pos=GetComponent<Rigidbody2D>().position;
        if (pos[0]>35  || pos[0]<-35 || pos[1] > 35 || pos[1] < -35) {
            print("Out");
            c.LoadScene("Menu");
        }
    }
}
